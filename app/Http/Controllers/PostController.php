<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access to the authenticated user via the Auth Facade
use Illuminate\Support\Facades\Auth;

// Implement the database manipulation
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // action to return a view containing form for a blog post creation.
    public function create() 
    {
        // 'posts' -> folder, 'create' -> file
        return view('posts.create');
    }

    // action to receive form data and subsequently store said data in the posts table.
    // Laravel's "Request" class contains the data from the form submission.
    public function store(Request $request)
    {
        // if there is an authenticated user.
        if (Auth::user()) {
            // Instantiate a new Post object from the Post model class.
            $post = new Post;
            // define the properties of the $post object using the received form data.
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->isActive = true;
            // get the id of the authenticated user and set it as the foreign key value of user_id of the new post.
            $post->user_id = (Auth::user()->id);
            
            // save this post object in the database
            $post->save();
            
            return redirect("/posts");
            }
            else {
                return redirect("/login");
            }
    }

    // action that will return a view showing all blog posts
    public function index() 
    {
        // Fetches all records from the table without any specific condition.
        // $posts = Post::all();

        // Fetches all records from the table with specific conditions (if need to build more complex queries).
        $posts = Post::where('isActive', true)
                ->get();

        // with() method is used to pass data to views file.
        // this assigns a variable name ('posts') and its corresponding data ($posts collection) to views.
        return view('posts.index')->with('posts', $posts);
    }

    // s02 Activity
    // action that will return a view showing three random blog post
    public function welcome() 
    {
        $random_posts = Post::where('isActive', true)
                        ->inRandomOrder()
                        ->limit(3)
                        ->get();

        return view('welcome')->with('random_posts', $random_posts);
    }

    // action for showing only the posts that is authored by the authenticated user.
    public function myPosts() 
    {
        if (Auth::user()) {
            // retrieve the posts authored by the current logged in user.
            // if you have succesfully established the model relationship, you can access related data.
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else {
            return redirect("/login");
        }
    }

    // action for showing a view of a specific post using the URL parameter 'id' to query for the database entry to be shown.
    // laravel automatically captures the value form the URL parameter and passes it as an argument to be the corresponding controller method.
    public function show($id) 
    {
        $post = Post::find($id);

        return view('posts.show')->with('post', $post);
    }

    //s03 activity
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        
        if(Auth::user()) {
            if(Auth::user()->id == $post->user_id){
                return view('posts.edit')->with('post', $post);
            }
            return redirect("/posts");
        }
        else {
            return redirect("/login");
        }
    }

    // action that will overwrite an existing post with matching id.
    public function update(Request $request, $id) 
    {
        $post = Post::find($id);

        // if authenticated user's ID is the same as the post's user_id
        if (Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            
            $post->save();

            return redirect("/posts");
        }
    }

    // action that will delete a specific post based on the given id.
    // public function destroy($id) 
    // {
    //     $post = Post::find($id);

    //     if (Auth::user()->id == $post->user_id) {
    //         $post->delete();
    //     }
    //     return redirect("/posts");
    // }
    

    // s04 activity
    public function archive($id) 
    {
        $post = Post::find($id);

        if (Auth::user()->id == $post->user_id) {
            $post->setAttribute('isActive', false);

            $post->save();
        }
        return redirect("/posts");
    }

    // action that will allow and authenticated user who is not the post author to toggle the like on the post being viewed.
    public function like($id) 
    {
        $post = Post::find($id);

        $user_id = Auth::user()->id;

        // if authenticated user is not the post author
        if ($post->user_id != $user_id) {
            // if the post like has been made by this user before.
            if ($post->likes->contains('user_id', $user_id)) {
                //unlike
                PostLike::where('post_id', $post->id)
                ->where('user_id', $user_id)
                ->delete();
            }
            else {
                // like
                $postLike = new PostLike;
                $postLike->post_id = $post->id;
                $postLike->user_id = $user_id;

                $postLike->save();
            }

            return redirect("/posts/$id");
        }
    }


    // s05 activity

    public function comment(Request $request, $id) 
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if (Auth::user()) {
            $postComment = new PostComment;
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->content = $request->input('content');
            $postComment->save();

            $comments = PostComment::where('post_id', $post->id)->get();

            return view('posts.show', compact('post', 'postComment', 'comments'));
        }

        return redirect("/posts/$id");
    }
}
    



