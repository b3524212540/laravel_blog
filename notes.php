====================SESSION 01====================

[SECTION] MVC Architecture

- Laravel is scalable and works with minimal configuration because it follows the "Model-View-Controller" architecture.

- "Model" component corresponds to all the data-related logic.

- "View" component is used for all the UI logic of the application.

- "Controllers" component act as an interface between Model and View components to process all the business logic and incoming requests, manipulate data using the Model component and interact with the Views to render the final output.


[SECTION] Create a laravel project

Syntax:

	composer create-project laravel/laravel <project-name>

After installation we can test if the application is running with the command:

	php artisan serve

	Note: Make sure that Apache and MySQL is started in the XAMPP.

[SECTION] Folder Structure

app folder
	- contains the models at its root. Models represent our database entities and have pre-defined methods for querying the respective tables that they represent.

	- "Http" -> Controllers subdirectory contains the project's controllers where we define our application/business logic.

database folder
	- "migrations subdirectory" contains the migrations that we will use to define the structures and data types of our database tables.

public folder
	- views subdirectory is the namespace where all views will be looked for by our application.

resources folder
	- where assets such as css, js, images, etc. can be stored and accessed.

routes folder
	- "web.php" file is where we define the routes of our web application.

.env file at the root of our project directory is where we set our application settings including database connection settings


[SECTION] Generate the authentication scaffolding

- Auth Scaffolding automatically creates a user registration, login, dashboard, logout, reset password and email verification with the help of Laravel UI package.

- install Laravel's laravel/ui package via the terminal command:

	composer require laravel/ui

- then build the authentication scaffolding via the terminal command (This wil create the login & register page):

	php artisan ui bootstrap --auth

- compile our fresh scaffolding with the command:

	npm install && npm run dev

	Note: npm run dev should be running when accessing the login and registration field.

[SECTION] Generating Model Classes with its corresponding migrations and controllers

	Syntax:

		php artisan make:model <ModelName> -mc

		- Model created is Eloquent Model(Eloquent is Laravel's ORM - Object Relational Model)
			- This will be used by laravel to map its relational database table.
			- Eloquent models have pre-defined methods database queries and operations.

		- Adding the "-mc" will also create the migrations and controller of the model
		- Migrations (m) are like version control for your database, allowing your team to define and share the application's database schema definition..
			- A migration class contains two methods:
				- "up" method is used to add new tables, columns, or indexes to your database
				- "down" method should reverse the operations performed by the up method.

		- controller (c) are meant to group associated request handling logic within a single class.

		- We can also create this component individually:
		 - php artisan make:migration <create_migration_name_table>

		 - php artisan make:controller <ControllerName>