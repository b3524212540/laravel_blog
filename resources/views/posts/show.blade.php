@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created At: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>

            @if (Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf

                    @if ($post->likes->contains('user_id', Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Comment
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Leave a comment</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body">
                            <form method="POST" action="/posts/{{$post->id}}/comment">
                                @method('POST')
                                @csrf

                                    <textarea
                                    class="form-control"
                                    name="content"
                                    rows="3"
                                    value="@if(isset($postComment)){{$postComment->content}}@endif
                                    ">
                                    </textarea>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Comment</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>

        </div>
    </div>

    @if (isset($comments))
        <h2>Comments:</h2>
            @foreach ($comments as $comment)
                <div class="card text-center mt-3">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ $comment->content }}</h4>
                        <h6 class="card-text mb-3">Author: {{ $comment->user->name }}</h6>
                        <p class="card-subtitle mb-3 text-muted">Created at: {{ $comment->created_at }}</p>
                    </div>
                </div>
            @endforeach
    @endif

@endsection