{{-- s02 Activity solution --}}

@extends('layouts.app')

@section('content')
    <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png"
        alt="laravel-logo"
        class="w-50 mx-auto d-block mb-4"
    >
    <h2 class="text-center mb-4">Featured Posts:</h2>
    @if ($random_posts) 
        @foreach ($random_posts as $random_post)
            <div class="card text-center mb-3">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$random_post->id}}">{{$random_post->title}}</a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{$random_post->user->name}}
                    </h6>
                </div>
            </div>
        @endforeach
    @else 
        <div>
            <h2>There are no posts to show.</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif
@endsection